%% Code to model 1D transient heat conduction using finite differences 
%% 2nd order approximation of laplacian(second order spatial derivative)
%% Theta method for time discretization

clear ;
close all ;

%% Model parameters 

% Diffusion constant 
dff = 0.01 ;
% Total simulation time 
T = 0.3;
% Timesteps 
Nt = 100 ;
% Domain size
L = 1 ;
% Number of spatial nodes
Nx = 20 ;
% Value of theta
theta = 0.2 ;

%% Dirichlet boundary conditions

% Temperature on left end
Tl = 1 ;
% Temperature on right end
Tr = 2 ;

%% Solve the problem
[x,uacc] = theimpl(dff,T,1000,L,Nx,theta,Tl,Tr) ; 

%% Plot
figure ;
hold on ;
plot(x,uacc,'b') ;
scatter(x,uacc,'filled','r') ;
grid on ;
xlabel('$x \rightarrow$','interpreter','latex') ;
ylabel('$T \rightarrow$','interpreter','latex') ;
fstr = sprintf('Temperature distribution at t = %4.2f',T) ;
title(fstr) ;
