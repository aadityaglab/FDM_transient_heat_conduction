function [x,u] = theimpl(dff,T,Nt,L,Nx,theta,Tl,Tr)

%% Implementing the theta method

% Spatial Grid size
dx = L/(Nx-1) ;
% Temporal grid size
dt = T/(Nt-1) ;
% CFL number
alp = dff*dt/(dx*dx) ;

% Generate spatial nodes
x = linspace(0,L,Nx)' ;

%% Initial conditions - Specify initial temperature distribution
u = linspace(Tl,Tr,Nx)'+0.4*sin(4*pi*x/L) ;

%% Boundary conditions augmented in matrices 

% Matrix to multiply with values from previous timestep
v1 = [ 1 ; (1 - 2*alp*(1 - theta))*ones(Nx-2,1) ; 1 ] ;
v2 = [0 ; alp*(1-theta)*ones(Nx-2,1)] ; 
rmat = diag(v1,0) + diag(v2,1) + diag(flipud(v2),-1) ; 
% Matrix to multiply with values from current timestep
w1 = [ 1 ; (1 + 2*alp*theta)*ones(Nx-2,1) ; 1 ] ;
w2 = [0 ; -alp*theta*ones(Nx-2,1)] ; 
lmat = diag(w1,0) + diag(w2,1) + diag(flipud(w2),-1) ; 

%% Plot sparsity patterns for lmat and rmat - Verification

% figure ;
% spy(lmat) ;
% figure ;
% spy(rmat) ;


% Compute matrix inverse

lmat = lmat\eye(Nx) ;
rmat = lmat*rmat ;

%% Simulate the heat transfer problem and plot the result 
% Note that because of constant diffusivity the matrices 
% constructed above need to be performed just once.
% This wouldn't be the case if the diffusivity was dependent 
% on temperature or time 

for i=1:Nt
u = rmat*u ; 
end 

end





